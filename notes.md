# Notes

### Guides
[Tutorial 1](https://fossheim.io/writing/posts/css-polaroid-camera/)

### Simple
![simple car model](simple-model.jpg)
- background
- body main; top corners rounded
- window containers
- half window x 2
- default window x 5
- stripes set of 6
- door containers
- door R x 2
- door L x 2
- quarter window
- bottom rail
